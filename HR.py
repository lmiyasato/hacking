#!/usr/bin/env python3

import csv
import sys
from itertools import chain, repeat


main_prompt = "\n".join(("Welcome to the Seahawks HR roster",
	"Please choose from the below options",
	"1: Print roster",
	"2: Add player to roster",
	"3: Remove player from roster",
	"4: Exit ",
	"",
	">>> "))

headers = ("Num"'\t'"Player"'\t\t'"POS"'\t'"Height"'\t'"Weight"'\t'"DOB"'\t\t'"Exp"'\t'"SSN"'\t\t'"College")


def print_roster():
    with open('seahawks.csv', newline='') as csvfile:
        roster = csv.reader(csvfile, delimiter=',', quotechar='|')
        print(headers)
        for row in sorted(roster):
            print('\t'.join(row))



def input_player():
# Player_Num input
    while True:
        try:
            print()
            Player_Num = int(input("Enter New Player's Jersey Number: "))
        except ValueError:
            print()
        if Player_Num > 100 or Player_Num < 1:
            print("Jersey Number must be a numeric value between 1-99")
        else:
            break

    Player_Nme = input("Enter Player's Name: ")

# Player Position

#    POS = {'QB','RB','WR','FB','TE','OT','OG','C','DE','DT','NT','LB','OLB','ILB','CB','FS','SS','P','K'}
#    Player_POS = chain(["Enter Player's Position: "], repeat("Must be valid Position: "))
#    replies = map(input,Player_POS)
#    valid_response = next(filter(POS.__contains__,replies))
#    print(Player_POS)
    Player_POS = input("Enter Player's Position: ")


# Player Height
    while True:
        try:
            Player_Hgt = input("Enter Player's Height: ")
        except ValueError:
            print()
        else:
            break

# Player_Wgt input
    while True:
        try:
            Player_Wgt = int(input("Enter Player's Weight: "))
        except ValueError:
            print()
        if Player_Wgt > 400 or Player_Wgt < 100:
            print("C'mon man, put a realistic weight")
        else:
            break

# Player Date of Birth
    Player_DOB = input("Enter Player's Date of Birth: ")

# Player Experience input
    while True:
        try:
            Player_Exp = int(input("Enter Player's NFL Experience: "))
        except ValueError:
            print()
        if Player_Exp > 25:
            print("Really?  Even Tom Brady didn't play for that long.")
        elif Player_Exp < 0:
            print("It needs to be at least 0")
        else:
            break

# Input Player SSN
    Player_SSN = int(input("Enter Player's SSN: ")) 
#    while True:
#        try:
#            Player_SSN = int(input("Enter Player's SSN: "))            
#        except ValueError:
#            print("The SSN needs to be 9 numbers")
#        if len.Player_SSN != 9:
#            print("The SSN needs to be 9 numbers")
#        else:
#            break



    Player_Col = input("Enter Player's College: ")

    print(headers)
#    player_entry=Player_num
    print(Player_Num,'\t',Player_Nme,'\t\t',Player_POS,'\t',Player_Hgt,'\t',Player_Wgt,'\t',Player_DOB,'\t\t',Player_Exp,'\t',Player_SSN,'\t\t',Player_Col)
    player_entry = (Player_Num,Player_Nme,Player_POS,Player_Hgt,Player_Wgt,Player_DOB,Player_Exp,Player_SSN,Player_Col)
#    player_entry = ('\n',Player_Num,Player_Nme,Player_POS,Player_Hgt,Player_Wgt,Player_DOB,Player_Exp,Player_SSN,Player_Col)
    
    print(player_entry)
    with open('seahawks.csv', 'a', newline='\n') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(' ')
        writer.writerow(player_entry)


def remove_player():
    pass


def exit_program():
    print()
    print("Go Hawks!")
    sys.exit()			# Exits program

def main():
    while True:
        response = input(main_prompt)
        if response == "1":
            print_roster()
        elif response == "2":
            input_player()
        elif response == "3":
        	remove_player()
        elif response == "4":
            exit_program()
        else:
            print("Not a valid option, try again")


if __name__ == "__main__":
    main()