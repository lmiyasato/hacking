#!/usr/bin/env python3

'''
### Author: Lenny Miyasato
### 
### This is a ICMP fuzzing script.  


'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import binascii
import time
import signal
import ipaddress


'''
### Root Check - This script needs to be run with root level privileges
'''

if os.geteuid()==0:
    print ("\n[*] Running as root / su; passed root check\n")
else:
    print ("\n\033[91m [x] \033[95mThis script needs to be run with root/su level privleges")
    sys.exit()		# Exits Program


'''
### UnTweakable variables:  The following is NOT intended to be tweaked
'''
time_now = datetime.now()                   			# The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")  		# time for the filename convention

'''
### Tweakable variables:  The following is intended to be tweakable to your liking
'''
pcap_path = "/tmp/"										# This will set the pcap output path to the desired directory


'''
### This part gets input and also performs input validation
'''

def validate_ip_address(IP_target):
	while True:
		try:
			ip_object = ipaddress.ip_address(IP_target)
			print("The IP address ",IP_target," is valid.")
			break
		except ValueError:
			print("\033[91mThe IP address ",IP_target," is NOT valid. \033[97m  Restart script and enter valid IP address")
			sys.exit()			# Exits program

def validate_port(x):
   # using comaparision operator
	if 0 <= x <= 65536:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid port.\033[97m  Valid ports are numbers between 0-65536.  Restart script.')
		sys.exit()				# Exits program
		
def validate_payload(x):
   # using comaparision operator
	if 0 <= x <= 64000:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid payload size.\033[97m  Valid ports are numbers between 0-64000.  Restart script.')
		sys.exit()				# Exits program

def validate_icmptype(x):
   # using comaparision operator
	if 0 <= x <= 255:
		print(x,' is a valid ICMP type\033[97m' )
	else:
		print('\033[91m', x,' is not a valid payload size.\033[97m  Valid ports are numbers between 0-255.  Restart script.')
		sys.exit()				# Exits program
		
IP_target = input('Enter \033[92mTARGET IP\033[97m to fuzz with the TFTP protocol\n')
validate_ip_address(IP_target)

Payload_Size = int(input('Enter the \033[92mPayload Size\033[97m (1-64000) - Larger value means larger .pcap file\n'))
validate_payload(Payload_Size)

# Iterations = int(input('Enter the \033[92mNumber of iterations\033[97m (1-64000) - Larger value means larger .pcap file\n'))		# Will leave it at max of 64K for now
# validate_payload(Iterations)

'''
### This part brings up the menu driven prompt; will need to develop functions on the menu choices
'''

def tshark_launch(pcap_path,time_now,PCAP_Prefix):
    filename = PCAP_Prefix + "_" + time_now + ".pcap"
    tshark_cmd = "tshark -ni any" + ' -w ' + pcap_path + filename + "&"
    os.system(tshark_cmd)
    print("\n \033[92m [*] Ready...  \033[97m  Get Set...\n\n")
    time.sleep(1)
    print("\n \033[92m [*] GO!  \033[97m  \n\n")
    print("\n \033[92m [*] TSHARK - process successfully started\n \033[97m")

def tshark_process_kill():
        processname = "tshark"
        time.sleep(1)
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("\n \033[92m [*] TSHARK - process successfully terminated\n \033[97m " )
            os.system("chmod 755 " + pcap_path + "ICMP*.pcap")
        except:
            print("\n \033[91m [X] TSHARK - Uh oh... the process thing didnt work, troubleshoot futher\n \033[97m ")



os.system('clear')

print("\033[91mEnsure your system targeting is correct:\033[97m\n\n")
print("\033[97mThe TARGET IP that you will be fuzzing is                      : \033[92m", IP_target)
# print("\033[97mThe number of ITIERATIONS that will be used for the fuzzing is : \033[92m", Iterations, "\n")
print("\033[97mThe .pcap files will be stored at                              : \033[92m", pcap_path)


main_prompt = "\n".join(("\033[97m\n\nSelect the type of fuzzing you want to perform:\n\n",
	"\033[92mPlease choose from the below options:\n\033[97m",
	"1: ICMP Type",
	"2: ICMP Code",
	"3: ICMP Type & Code Combination (This will take a while, ~65K packets)",
	"4: ICMP Type & Code Combination of known Type & Code combinations",
	"5: Placeholder for future revisions (exit)",
	"6: Placeholder for future revisions (exit)",
	"7: Exit",					 
	"",
	">>> "))

'''
### Main Menu Function
'''

def main():
    while True:
        response = input(main_prompt)
        if response == "1":
            ICMP_Type()
        elif response == "2":
            ICMP_Code()
        elif response == "3":
            ICMP_TypeCode()
        elif response == "4":
            ICMP_KnownTypeCode()
        elif response == "5":
            Packet_Error()		
        elif response == "6":
            Packet_Random()				
        elif response == "7":
            exit_program()
        else:
            print("Not a valid option, try again")

# These sets of variables sets up the protocol level (TFTP) settings
		

def ICMP_Type():
    PCAP_Prefix = "ICMP_Type"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    for ICMPtype in range(0,254):
        packet = IP(dst=IP_target)/ICMP(type=ICMPtype)/Raw(os.urandom(Payload_Size))
        send(packet)
    tshark_process_kill()

def ICMP_Code():
    PCAP_Prefix = "ICMP_Code"
    ICMP_type = int(input('Enter the \033[92mICMP type\033[97m (0-255)\n'))
    validate_icmptype(ICMP_type)
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    for ICMPcode in range(0,254):
        packet = IP(dst=IP_target)/ICMP(type=ICMP_type,code=ICMPcode)/Raw(os.urandom(Payload_Size))
        send(packet)
    tshark_process_kill()	
	
def ICMP_TypeCode():
    PCAP_Prefix = "ICMP_TypeCode"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    for ICMPtype in range(0,254):
        for ICMPcode in range(0,254):
            packet = IP(dst=IP_target)/ICMP(type=ICMPtype,code=ICMPcode)/Raw(os.urandom(Payload_Size))
            send(packet)
    tshark_process_kill()
    


def ICMP_KnownTypeCode():
    PCAP_Prefix = "ICMP_KnownTypeCode"
    # The known codes are from documented type/code combinations noted in RFC 6918; but derived from wireshark.
    Known_Codes = {0:0,1:0,2:0,3:0,3:1,3:2,3:3,3:4,3:5,3:6,3:7,3:8,3:9,3:10,3:11,3:12,3:13,3:14,3:15,4:0,5:0,5:1,5:2,5:3,6:0,8:0,9:0,9:16,10:0,11:0,11:1,12:0,12:1,12:2,13:0,14:0,15:0,16:0,17:0,18:0,19:0,30:0,31:0,32:0,33:0,34:0,35:0,36:0,37:0,38:0,39:0,40:0,40:1,40:2,40:3,40:4,40:5,41:0,42:0,43:0,43:1,43:2,43:3,43:4}
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    for icmp_type, icmp_code in Known_Codes.items():
        packet = IP(dst=IP_target)/ICMP(type=icmp_type,code=icmp_code)/Raw(os.urandom(Payload_Size))
        send(packet)
    tshark_process_kill()
	
def Packet_Error():
    print()
    print("\033[97m \n[*] Your .pcap files are located at: \033[92m",pcap_path)
    sys.exit()			# Exits program
	
def Packet_Random():
    print()
    print("\033[97m \n[*] Your .pcap files are located at: \033[92m",pcap_path)
    sys.exit()			# Exits program
	
	
	
	
'''
### This function executes the exit program
'''

def exit_program():
    print()
    print("\033[97m \n[*] Your .pcap files are located at: \033[92m",pcap_path)
    sys.exit()			# Exits program


main()









