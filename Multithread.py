#!/usr/bin/env python3

import threading
import time


def eat_breakfast():
    print("Breakfast Eating Started")
    time.sleep(3)
    print("Breakfast is done")

def drink_coffee():
    print("Coffee Drinking Started")
    time.sleep(4)
    print("Coffee is done")

def study():
    print("Studying Started")
    time.sleep(5)
    print("Studying is done")

x = threading.Thread(target=eat_breakfast, args=())
x.start()

y = threading.Thread(target=eat_breakfast, args=())
y.start()

z = threading.Thread(target=eat_breakfast, args=())
z.start()

x.join()
y.join()
z.join()

print(threading.active_count())
print(threading.enumerate())
print(time.perf_counter())