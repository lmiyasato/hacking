#!/usr/bin/env python

'''
### Author: Lenny Miyasato
### 
### The purpose of this script is to help conduct exploratory testing.  It is intended for the operator to set the variables in the variable section and the output will generate files
### in a naming convention.  More info to be added when I finish the concept.
'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import binascii
import time
import signal


'''
# This defines the Source/Target MAC, Target & Port(s)
'''

mac_src="82:ac:18:14:0b:00"			            	# Sets your interface to the defined MAC address
ip_src="192.168.15.133"				            	# Sets your interface to the defined IP address
ip_dst="192.168.15.138"				                # Sets the IP target that you are testing
interface="eth1"				                # Set this to the interface that you are testing with
pcap_path="/tmp"				                # Sets temporary path to store output files; ensure you have write privs to it
src_host="TEST"                  		        	# For filename convention for source hostname
src_target="TFTP-SERVER"                			# For filename convention for target hostname
state="PARKED"                          	    		# For filename convention [state|flight]
time_now = datetime.now()                   			# The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M")  		# time for the filename convention


'''
### These will define various script variables
'''

### This defines the tshark function
def tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc):
    filename = src_host + "_to_" + src_target + "_" + state + "_" + tool + "_" + protocol + "_" + time_now + "_" + desc + extension
    tshark_cmd = "tshark -i" + interface + ' -w ' + pcap_path + "/" + filename + "&"
    os.system(tshark_cmd)
    print("\n \033[92m [*] TSHARK - process successfully started\n \033[97m")


### This section defines the character ramdomization to create payloads of random characters; not used but kept for reference
def randstr(chars = string.ascii_uppercase + string.digits, N=10):
    return ''.join(random.choice(chars) for _ in range(N))
big_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=50000))
med_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=10000))
sml_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=100))
opc_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=2))
hexlist = ["{:#04x}".format(x) for x in range(16)] + [hex(x) for x in range(16, 256)]

### This dection defines the function to kill a process; used to kill tshark after a command is executed
def process_kill():
        processname = "tshark"
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("\n \033[92m [*] TSHARK - process successfully terminated\n \033[97m ")
        except:
            print("\n \033[91m [X] TSHARK - Uh oh... the process thing didnt work, troubleshoot futher\n \033[97m ")



def settings_confirmation(ip_src,ip_dst,interface,pcap_path,src_host,src_target,state):
    clear = "clear"
    os.system(clear)
    print("\n \033[94m [*] Please validate your testing script configuration:  \n\033[97m  \n")
    mac_confirm = "ethtool -P " + interface
    print("Your MAC address is:\n\033[92m") 
    os.system(mac_confirm)
    print("\n \033[97m Source IP address is:\033[92m		" + ip_src)
    print("\n \033[97m Target IP address is:\033[92m		" + ip_dst)
    print("\n \033[97m Interface is:\033[92m			" + interface)            
    print("\n \033[97m Output path is:\033[92m		" + pcap_path)
    print("\n \033[97m Source hostname is:\033[92m		" + src_host) 
    print("\n \033[97m Target hostname is:\033[92m		" + src_target)
    print("\n \033[97m State (Parked|Flight) is:\033[92m	" + state)           
    print("\n\n\n")
    input("Press any key to continue")
    
settings_confirmation(ip_src,ip_dst,interface,pcap_path,src_host,src_target,state)


''' #####################################################################################################################
### 		This section is for SCAPY/FUZZ tests
''' #####################################################################################################################


'''
### src_port=		- Configure to desired source port		
### dst_port=		- Configure to desired tftp listening port
### protocol=		- Used for .pcap naming.  Normally use TCP or UDP
### tool=		- Used for tool used, Normally use FUZZ
### desc=		- Used for additional descriptors
### extension=".pcap"  	- Leave it as is
'''


src_port=61695
dst_port=69
protocol="UDP"
tool="FUZZ"
desc="TFTP_TEST"
extension=".pcap"


# These sets of variables sets up the protocol level (TFTP) settings

'''
### opcode=		- Used for TFTP opcode settings, do not alter.		
### tftp_cmd=		- Used for TFTP trailer codes, do not alter.
### payload=		- Used for sending a stream of single charagers, not used by default
### null=		- Used used to send a null byte
'''

opcode_rrq= '\x00\x01'
opcode_wrq= '\x00\x02'
opcode_data= '\x00\x03'
opcode_ack= '\x00\x04'
opcode_err= '\x00\x05'
tftpcmd_ascii= '\x00\x61\x73\x63\x69\x69\x00'
tftpcmd_binary= '\x00\x62\x69\x6e\x61\x72\x79\x00'
tftpcmd_connect='\x00\x63\x6f\x6e\x6e\x65\x63\x74\x00'
tftpcmd_get='\x00\x90\x00\x67\x65\x74\x00'
tftpcmd_mode='\x00\x6d\x6f\x64\x65\x00'
tftpcmd_netascii='\x00\x6e\x65\x74\x61\x73\x63\x69\x69\x00'
tftpcmd_put='\x00\x70\x75\x74\x00'
tftpcmd_quit='\x00\x71\x75\x69\x74'
tftpcmd_rexmnt='\x00\x70\x75\x74\x00'
tftpcmd_rrq='\x00\x72\x72\x71\x00'
tftpcmd_status='\x00\x73\x74\x61\x74\x75\x73\x00'
tftpcmd_timeout='\x00\x74\x69\x6d\x65\x6f\x75\x74\x00'
tftpcmd_trace='\x00\x74\x72\x61\x63\x65\x00'
tftpcmd_verbose='\x00\x76\x65\x72\x62\x6f\x73\x65\x00'
tftp_error_01= '\x00\x01'
tftp_error_02= '\x00\x02'
tftp_error_03= '\x00\x03'
tftp_error_04= '\x00\x04'
tftp_error_05= '\x00\x05'
tftp_error_06= '\x00\x06'
tftp_error_07= '\x00\x07'

payload= 'A' * 60000
null = '\x00'


print ("\n \033[96m [*] LAUNCHING the" + " " + protocol + " " + tool + " " + desc + " test. \n\033[97m")
tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc)
time.sleep(1)


### This sends malformed TFTP packets to the target.  Not sure what it does to the target.

opcode = ((opcode_rrq), (opcode_wrq), (opcode_data), (opcode_ack), (opcode_err)) 

tftpcmd = ((tftpcmd_ascii), (tftpcmd_binary), (tftpcmd_connect), (tftpcmd_get), (tftpcmd_mode), (tftpcmd_netascii), (tftpcmd_put), (tftpcmd_quit), (tftpcmd_rexmnt), (tftpcmd_rrq), (tftpcmd_status), (tftpcmd_timeout), (tftpcmd_trace), (tftpcmd_verbose)) 

tftperr = ((tftp_error_01), (tftp_error_02), (tftp_error_03), (tftp_error_04), (tftp_error_05), (tftp_error_06), (tftp_error_07)) 

'''
### 	The first FOR-LOOP runs a fuzzer following the TFTP protocol based on RFC 1782 (at least tries to...)
### 	https://www.rfc-editor.org/rfc/rfc1782.txt
###
###	You can alter the middle "Raw" value to adjust the payload size.  But don't modify the os.random(2).
'''


packet_wrq = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_wrq)/Raw(payload)/Raw(null)/Raw(tftpcmd_trace)/Raw(null)
send(packet_wrq, count=1)


'''
for x in opcode:
	for y in tftpcmd:
#		packet_rrq = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_rrq)/Raw(os.urandom(60))/Raw(y)
		packet_wrq = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_wrq)/Raw(payload)		
#		send(packet_rrq, count=1)
		send(packet_wrq, count=1)
'''
'''
for x in range (1,3,1):
#	packet_data = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_data)/Raw(payload)
#	packet_ack = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_ack)/Raw(os.urandom(2))
#	packet_err = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_err)/Raw(os.urandom(2))/Raw(os.urandom(60))/Raw(null)
	srloop(packet_data, count=1)
#	srloop(packet_ack, count=1)
#	srloop(packet_err, count=1)

'''



'''
### 	The second FOR-LOOP runs a fuzzer following the TFTP protocol but doesn't really follow the RFC.
### 	
###	You can alter the sizes for the "os.urandom" values, but don't exceed a total of 65535 for all three values
###	or the scapy will break and not run.  Also the first os.urandom "should" be 2 to follow the RFC.
'''
'''
for x in range (1,1,1):
	packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(os.urandom(2))/Raw(null)/Raw(os.urandom(65000))/Raw(null)/Raw(os.urandom(16))
	srloop(packet, count=1)
'''



time.sleep(1)
process_kill()


print ("\n \033[96m [*] COMPLETED the" + " " + protocol + " " + tool + " " + desc + " test. \n\033[97m")






'''
## SCAPY SNIPPET Recycle bin

packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/big_payload		# This setups the payload
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_ACK(block=50)
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_DATA(block=50)
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_ERROR(errorcode=2,errormsg="ERROR")
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_OACK()
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_Option(oname="AA", value="BB")
srloop(packet, count=10)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_Options(options=["AAAAAAAAAAAA "] )
srloop(packet, count=10)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_RRQ(filename='test.txt',mode='octet')
srloop(packet, count=10)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_WRQ(filename="test.txt",mode=b'octet')
srloop(packet, count=10)	
'''







