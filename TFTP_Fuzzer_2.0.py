#!/usr/bin/env python3

'''
### Author: Lenny Miyasato
### 
### This is a different version of the TFTP fuzzing script.  The goal of this script is to make a menu-driven version of the TFTP 
### fuzzing script.


'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import binascii
import time
import signal
import ipaddress


'''
### Root Check - This script needs to be run with root level privileges
'''

if os.geteuid()==0:
    print ("\n[*] Running as root / su; passed root check\n")
else:
    print ("\n\033[91m [x] \033[95mThis script needs to be run with root/su level privleges")
    sys.exit()		# Exits Program


'''
### UnTweakable variables:  The following is NOT intended to be tweaked
'''
time_now = datetime.now()                   			# The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")  		# time for the filename convention

'''
### Tweakable variables:  The following is intended to be tweakable to your liking
'''
pcap_path = "/tmp/"										# This will set the pcap output path to the desired directory


'''
### This part gets input and also performs input validation
'''

def validate_ip_address(IP_target):
	while True:
		try:
			ip_object = ipaddress.ip_address(IP_target)
			print("The IP address ",IP_target," is valid.")
			break
		except ValueError:
			print("\033[91mThe IP address ",IP_target," is NOT valid. \033[97m  Restart script and enter valid IP address")
			sys.exit()			# Exits program



def validate_port(x):
   # using comaparision operator
	if 0 <= x <= 65536:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid port.\033[97m  Valid ports are numbers between 0-65536.  Restart script.')
		sys.exit()				# Exits program
		
def validate_payload(x):
   # using comaparision operator
	if 0 <= x <= 64000:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid payload size.\033[97m  Valid ports are numbers between 0-64000.  Restart script.')
		sys.exit()				# Exits program
		
IP_target = input('Enter \033[92mTARGET IP\033[97m to fuzz with the TFTP protocol\n')
validate_ip_address(IP_target)

Source_Port = int(input('Enter the \033[92mSOURCE PORT\033[97m to be used\n'))     # Make port # validation thingy
validate_port(Source_Port)

Target_Port = int(input('Enter the \033[92mTARGET PORT\033[97m to be used\n'))
validate_port(Target_Port)

Payload_Size = int(input('Enter the \033[92mPayload Size\033[97m (1-64000) - Larger value means larger .pcap file\n'))
validate_payload(Payload_Size)

Iterations = int(input('Enter the \033[92mNumber of iterations\033[97m (1-64000) - Larger value means larger .pcap file\n'))		# Will leave it at max of 64K for now
validate_payload(Iterations)

'''
### This part brings up the menu driven prompt; will need to develop functions on the menu choices
'''

def tshark_launch(pcap_path,time_now,TFTP_type):
    filename = TFTP_type + "_" + time_now + ".pcap"
    tshark_cmd = "tshark -ni any" + ' -w ' + pcap_path + filename + "&"
    os.system(tshark_cmd)
    print("\n \033[92m [*] Ready...  \033[97m  Get Set...\n\n")
    time.sleep(1)
    print("\n \033[92m [*] GO!  \033[97m  \n\n")
    print("\n \033[92m [*] TSHARK - process successfully started\n \033[97m")

def tshark_process_kill():
        processname = "tshark"
        time.sleep(1)
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("\n \033[92m [*] TSHARK - process successfully terminated\n \033[97m " )
            os.system("chmod 755 " + pcap_path + "TFTP*.pcap")
        except:
            print("\n \033[91m [X] TSHARK - Uh oh... the process thing didnt work, troubleshoot futher\n \033[97m ")



os.system('clear')

print("\033[91mEnsure your system targeting is correct:\033[97m\n\n")
print("\033[97mThe TARGET IP that you will be fuzzing is                      : \033[92m", IP_target)
print("\033[97mThe SOURCE PORT that will be used for the fuzzing is           : \033[92m", Source_Port)
print("\033[97mThe TARGET PORT that will be used for the fuzzing is           : \033[92m", Target_Port, "\n\n")
print("\033[97mThe PAYLOAD SIZE that will be used for the fuzzing is          : \033[92m", Payload_Size)
print("\033[97mThe number of ITIERATIONS that will be used for the fuzzing is : \033[92m", Iterations, "\n")
print("\033[97mThe .pcap files will be stored at                              : \033[92m", pcap_path)


main_prompt = "\n".join(("\033[97m\n\nSelect the type of fuzzing you want to perform:\n\n",
	"\033[92mPlease choose from the below options:\n\033[97m",
	"1: TFTP Packet RRQ (Read Request Fuzzing)",
	"2: TFTP Packet WRQ (Write Request Fuzzing)",
	"3: TFTP Packet Data Fuzzing",
	"4: TFTP Packet Acknowledgement",
	"5: TFTP Packet Error Fuzzing",
	"6: TFTP Random Fuzzing within the TFTP protocol",
	"7: Exit",					 
	"",
	">>> "))

'''
### Main Menu Function
'''

def main():
    while True:
        response = input(main_prompt)
        if response == "1":
            Packet_RRQ()
        elif response == "2":
            Packet_WRQ()
        elif response == "3":
            Packet_Data()
        elif response == "4":
            Packet_Ack()
        elif response == "5":
            Packet_Error()		
        elif response == "6":
            Packet_Random()				
        elif response == "7":
            exit_program()
        else:
            print("Not a valid option, try again")

# These sets of variables sets up the protocol level (TFTP) settings

'''
### opcode=			- Used for TFTP opcode settings, do not alter.		
### tftp_cmd=		- Used for TFTP trailer codes, do not alter.
### payload=		- Used for sending a stream of single charagers, not used by default
### null=			- Used used to send a null byte
'''

opcode_rrq= '\x00\x01'
opcode_wrq= '\x00\x02'
opcode_data= '\x00\x03'
opcode_ack= '\x00\x04'
opcode_err= '\x00\x05'
tftpcmd_ascii= '\x00\x61\x73\x63\x69\x69\x00'
tftpcmd_binary= '\x00\x62\x69\x6e\x61\x72\x79\x00'
tftpcmd_connect='\x00\x63\x6f\x6e\x6e\x65\x63\x74\x00'
tftpcmd_get='\x00\x90\x00\x67\x65\x74\x00'
tftpcmd_mode='\x00\x6d\x6f\x64\x65\x00'
tftpcmd_netascii='\x00\x6e\x65\x74\x61\x73\x63\x69\x69\x00'
tftpcmd_put='\x00\x70\x75\x74\x00'
tftpcmd_quit='\x00\x71\x75\x69\x74'
tftpcmd_rexmnt='\x00\x70\x75\x74\x00'
tftpcmd_rrq='\x00\x72\x72\x71\x00'
tftpcmd_status='\x00\x73\x74\x61\x74\x75\x73\x00'
tftpcmd_timeout='\x00\x74\x69\x6d\x65\x6f\x75\x74\x00'
tftpcmd_trace='\x00\x74\x72\x61\x63\x65\x00'
tftpcmd_verbose='\x00\x76\x65\x72\x62\x6f\x73\x65\x00'
tftp_error_01= '\x00\x01'
tftp_error_02= '\x00\x02'
tftp_error_03= '\x00\x03'
tftp_error_04= '\x00\x04'
tftp_error_05= '\x00\x05'
tftp_error_06= '\x00\x06'
tftp_error_07= '\x00\x07'
# payload= 'A' * 60
null = '\x00'

### This sends malformed TFTP packets to the target.  Not sure what it does to the target.

opcode = ((opcode_rrq), (opcode_wrq), (opcode_data), (opcode_ack), (opcode_err)) 
tftpcmd = ((tftpcmd_ascii), (tftpcmd_binary), (tftpcmd_connect), (tftpcmd_get), (tftpcmd_mode), (tftpcmd_netascii), (tftpcmd_put), (tftpcmd_quit), (tftpcmd_rexmnt), (tftpcmd_rrq), (tftpcmd_status), (tftpcmd_timeout), (tftpcmd_trace), (tftpcmd_verbose)) 
tftperr = ((tftp_error_01), (tftp_error_02), (tftp_error_03), (tftp_error_04), (tftp_error_05), (tftp_error_06), (tftp_error_07)) 
			

def Packet_RRQ():
    TFTP_type = "TFTP_RRQ"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,TFTP_type)
    for y in tftpcmd:
        packet_rrq = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(opcode_rrq)/Raw(os.urandom(Payload_Size))/Raw(y)
        send(packet_rrq, count=Iterations)
    tshark_process_kill()

def Packet_WRQ():
    TFTP_type = "TFTP_WRQ"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,TFTP_type)
    for y in tftpcmd:
        packet_wrq = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(opcode_wrq)/Raw(os.urandom(Payload_Size))/Raw(y)
        send(packet_wrq, count=Iterations)
    tshark_process_kill()	
	
def Packet_Data():
    TFTP_type = "TFTP_Data"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,TFTP_type)
    for x in range(Iterations):
        packet_data = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(opcode_data)/Raw(os.urandom(Payload_Size))
        send(packet_data, count=1)
    tshark_process_kill()	
	

def Packet_Ack():
    TFTP_type = "TFTP_Ack"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,TFTP_type)
    for x in range(Iterations):
        packet_data = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(opcode_ack)/Raw(null)/Raw(os.urandom(Payload_Size))/Raw(null)/Raw(os.urandom(16))
        send(packet_data, count=1)
    tshark_process_kill()		
	
def Packet_Error():
    TFTP_type = "TFTP_Error"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,TFTP_type)
    for x in range(Iterations):
        packet_data = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(opcode_err)/Raw(null)/Raw(os.urandom(Payload_Size))/Raw(null)/Raw(os.urandom(16))
        send(packet_data, count=1)
    tshark_process_kill()	
	
def Packet_Random():
    TFTP_type = "TFTP_Random"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,TFTP_type)
    for x in range(Iterations):
        packet_data = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(os.urandom(2))/Raw(null)/Raw(os.urandom(Payload_Size))/Raw(null)/Raw(os.urandom(16))
        send(packet_data, count=1)
    tshark_process_kill()	
	
	
	
	
'''
### This function executes the exit program
'''

def exit_program():
    print()
    print("\033[97m \n[*] Your .pcap files are located at: \033[92m",pcap_path)
    sys.exit()			# Exits program


main()









