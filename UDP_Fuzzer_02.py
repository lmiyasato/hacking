#!/usr/bin/env python3

'''
### Author: Lenny Miyasato
### 
### This is a Generic UDP fuzzing script.  


'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import binascii
import time
import signal
import ipaddress


'''
### Root Check - This script needs to be run with root level privileges
'''

if os.geteuid()==0:
    print ("\n[*] Running as root / su; passed root check\n")
else:
    print ("\n\033[91m [x] \033[95mThis script needs to be run with root/su level privleges")
    sys.exit()		# Exits Program


'''
### UnTweakable variables:  The following is NOT intended to be tweaked
'''
time_now = datetime.now()                   			# The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")  		# time for the filename convention

'''
### Tweakable variables:  The following is intended to be tweakable to your liking
'''
pcap_path = "/tmp/"										# This will set the pcap output path to the desired directory


'''
### This part gets input and also performs input validation
'''

def validate_ip_address(IP_target):
	while True:
		try:
			ip_object = ipaddress.ip_address(IP_target)
			print("The IP address ",IP_target," is valid.")
			break
		except ValueError:
			print("\033[91mThe IP address ",IP_target," is NOT valid. \033[97m  Restart script and enter valid IP address")
			sys.exit()			# Exits program

def validate_port(x):
   # using comaparision operator
	if 0 <= x <= 65536:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid port.\033[97m  Valid ports are numbers between 0-65536.  Restart script.')
		sys.exit()				# Exits program
		
def validate_payload(x):
   # using comaparision operator
	if 0 <= x <= 64000:
		print(x,' is a valid port\033[97m' )
	else:
		print('\033[91m', x,' is not a valid payload size.\033[97m  Valid ports are numbers between 0-64000.  Restart script.')
		sys.exit()				# Exits program

        
def destport_file_check():
    with open('dest_ports.txt') as file:
        for item in file:
            if re.match('^([0-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$', item):
                None
            else:
                print("\033[97mInput file is NOT valid.  Go fix it.  The bad line is:\033[92m",item)
                sys.exit()
    print("\033[97m File Validation for dest_ports.txt inputfile is:   \033[92m GOOD \033[97m")
    file.close()

def sourceport_file_check():
    with open('source_ports.txt') as file:
        for item in file:
            if re.match('^([0-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$', item):
                None
            else:
                print("\033[97mInput file is NOT valid.  Go fix it.  The bad line is:\033[92m",item)
                sys.exit()
    print("\033[97m File Validation for source_ports.txt inputfile is: \033[92m GOOD \033[97m")
    file.close()


def tshark_launch(pcap_path,time_now,PCAP_Prefix):
    filename = PCAP_Prefix + "_" + time_now + ".pcap"
    tshark_cmd = "tshark -ni any" + ' -w ' + pcap_path + filename + "&"
    os.system(tshark_cmd)
    print("\n \033[92m [*] Ready...  \033[97m  Get Set...\n\n")
    time.sleep(1)
    print("\n \033[92m [*] GO!  \033[97m  \n\n")
    print("\n \033[92m [*] TSHARK - process successfully started\n \033[97m")

def tshark_process_kill():
        processname = "tshark"
        time.sleep(1)
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("\n \033[92m [*] TSHARK - process successfully terminated\n \033[97m " )
            os.system("chmod 755 " + pcap_path + "UDP*.pcap")
        except:
            print("\n \033[91m [X] TSHARK - Uh oh... the process thing didnt work, troubleshoot futher\n \033[97m ")

		
IP_target = input('Enter \033[92mTARGET IP\033[97m to fuzz with the TFTP protocol\n')
validate_ip_address(IP_target)

Source_Port = int(input('Enter the \033[92mSOURCE PORT\033[97m to be used\n'))     # Make port # validation thingy
validate_port(Source_Port)

Target_Port = int(input('Enter the \033[92mTARGET PORT\033[97m to be used\n'))
validate_port(Target_Port)

Payload_Size = int(input('Enter the \033[92mPayload Size\033[97m (1-64000) - Larger value means larger .pcap file\n'))
validate_payload(Payload_Size)

Iterations = int(input('Enter the \033[92mNumber of iterations\033[97m - Larger value means larger .pcap file\n'))		

'''
### This part brings up the menu driven prompt; will need to develop functions on the menu choices
'''


os.system('clear')

print("\033[91mEnsure your system targeting is correct:\033[97m\n\n")
print("\033[97mThe TARGET IP that you will be fuzzing is                      : \033[92m", IP_target)
# print("\033[97mThe number of ITIERATIONS that will be used for the fuzzing is : \033[92m", Iterations, "\n")
print("\033[97mThe .pcap files will be stored at                              : \033[92m", pcap_path)


main_prompt = "\n".join(("\033[97m\n\nSelect the type of fuzzing you want to perform:\n\n",
	"\033[92mPlease choose from the below options:\n\033[97m",
	"1: UDP - Single Port Random Data Payload Fuzz",
	"2: UDP - Random Source & Destination Port & Random Data Payload Size Fuzz",
	"3: UDP - Single Port Flood",
	"4: UDP - Multiport (source_ports.txt & dest_ports.txt config file required)",
	"5: Placeholder for future revisions (exit)",
	"6: Placeholder for future revisions (exit)",
	"7: Exit",					 
	"",
	">>> "))

'''
### Main Menu Function
'''

def main():
    while True:
        response = input(main_prompt)
        if response == "1":
            UDP_SinglePort()
        elif response == "2":
            UDP_RandomPorts()
        elif response == "3":
            UDP_SinglePortFlood()
        elif response == "4":
            UDP_MultiPort()
        elif response == "5":
            exit_program()		
        elif response == "6":
            exit_program()				
        elif response == "7":
            exit_program()
        else:
            print("Not a valid option, try again")

# These sets of variables sets up the protocol level (TFTP) settings

def UDP_SinglePort():
    PCAP_Prefix = "UDP_SinglePort"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    counter = int(0)
    for counter in range(0,Iterations):
            packet = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(os.urandom(Payload_Size))
            print("\033[32m [X] Sent packet ",counter," of " ,Iterations, " \033[97m ")
            send(packet)
            counter = counter + 1
    tshark_process_kill()
	

def UDP_RandomPorts():
    PCAP_Prefix = "UDP_RandomPorts"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    counter = int(0)
    for counter in range(0,Iterations):
            packet = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(random.randint(0,65535)), dport=(random.randint(0,65535)))/Raw(os.urandom(random.randint(0,64000)))
            print("\033[32m [X] Sent packet ",counter," of " ,Iterations, " \033[97m ")
            send(packet)
            counter = counter + 1
    tshark_process_kill()
	
def UDP_SinglePortFlood():
    PCAP_Prefix = "UDP_SinglePortFlood"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    PacketCount = int(input('Enter the \033[92mNumber of packets to be send: \033[97m - Larger value means larger .pcap file\n'))		
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    packet = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(Source_Port), dport=(Target_Port))/Raw(os.urandom(Payload_Size))
    print("\033[32m  [X] Sending Packets...", "\033[97m ")
    send(packet,count=(PacketCount),verbose=1)
    tshark_process_kill()
	

def UDP_MultiPort():
    PCAP_Prefix = "UDP_MultiPort"
    time_now = datetime.now()
    time_now = time_now.strftime("%Y-%m-%d-%H-%M-%S")
    tshark_launch(pcap_path,time_now,PCAP_Prefix)
    sourceport_file_check()	
    destport_file_check()
    with open ('source_ports.txt') as file:
        for sourceports in file:
            with open ('dest_ports.txt') as file2:
                for destports in file2:
                    counter = int(0)
                    for counter in range(0,Iterations):
                        packet = IP(src=(get_if_addr(conf.iface)), dst=(IP_target))/UDP(sport=int(sourceports), dport=int(destports))/Raw(os.urandom(Payload_Size))
                        print("\033[32m [X] Sent packet ",counter," of " ,Iterations, " \033[97m ")
                        send(packet)
                        counter = counter + 1
    tshark_process_kill()
   



# This function executes the exit program


def exit_program():
    print()
    print("\033[97m \n[*] Your .pcap files are located at: \033[92m",pcap_path)
    sys.exit()			# Exits program


main()









