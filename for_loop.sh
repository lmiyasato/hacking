#/bin/bash

while read i; do
	nc -n -v -l -k -p "$i" &
	nc -n -v -l -k -u -p "$i" &
done <ports.txt
