#!/usr/bin/env python3

import ipaddress

IP="127.0.0.55"

def validate_ip_address(IP):
	try:
		ip_object = ipaddress.ip_address(IP)
		print("The IP address ",IP," is valid.")
	except ValueError:
		print("The IP address ",IP," is not valid.")
		
validate_ip_address(IP)


			