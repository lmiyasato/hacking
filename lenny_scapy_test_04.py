#!/usr/bin/env python

'''
### Author: Lenny Miyasato
### 
### The purpose of this script is to help conduct exploratory testing.  It is intended for the operator to set the variables in the variable section and the output will generate files
### in a naming convention.  More info to be added when I finish the concept.
'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import time
import signal


'''
# This defines the Source/Target MAC, Target & Port(s)
'''

mac_src="82:ac:18:14:0b:00"
ip_src="172.24.20.11"
ip_dst="192.168.10.129"
interface="eth0"
pcap_path="/tmp"
src_host="FMC-L"                  # For filename convention for source hostname
src_target="NFS2-L"                # For filename convention for target hostname
state="PARKED"                          # For filename convention [state|flight]
time_now = datetime.now()                   # The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M")   # time for the filename convention


'''
### These will define various script variables
'''
# test_case_name = (src_host) + "_to_" + (src_target) + (state) + "_fuzz_" + "TCP_" + (time_now)
# tshark_launch=('tshark -i ') + (interface) + (' -w ') + (pcap_path) + '/' + (test_case_name) + ('.pcap') + ('&')

def tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc):
    filename = src_host + "_to_" + src_target + "_" + state + "_" + tool + "_" + protocol + "_" + desc + "_" + time_now + extension
    cmd = "tshark -i" + interface + ' -w ' + pcap_path + "/" + filename + "&"
    os.system(cmd)



'''
### This section defines the character ramdomization to create payloads of random characters
'''

def randstr(chars = string.ascii_uppercase + string.digits, N=10):
    return ''.join(random.choice(chars) for _ in range(N))
    
big_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=30000))
med_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=10000))
sml_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=100))

'''
### This dection defines the function to kill a process; used to kill tshark after a command is executed
'''

def process_kill():
        processname = "tshark"
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("[*] process successfully terminated")
        except:
            print("[X] Uh oh... the process thing didnt work, troubleshoot futher")


'''
### This changes the MAC address
'''

def changemac(mac_src,interface):

    mac_down = "ifconfig " + interface + " down" 
    mac_change = "ifconfig " + interface + " hw ether " + mac_src
    mac_up = "ifconfig " + interface + " up " + " & " + "ifconfig " + interface + " |grep ether" 
    os.system(mac_down)
    os.system(mac_change)
    os.system(mac_up)
    time.sleep(1)

changemac(mac_src,interface)

''' 
### This section defines and executes the test cases
'''

# This sends a layer 4 packet without custom MAC
src_port=61001
dst_port=59
protocol="UDP"
tool="FUZZ"
desc="FUZZ1"
extension=".pcap"

tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc)
time.sleep(3)
dst_port_range = [53000,53001,53002,53003,53004,53005,53006,53007,53008,53009,53010]
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port_range))
srloop(packet, count=5)
print(packet.summary)
time.sleep(3)
process_kill()



# This sends a fuzzed packet at the layer 4 level to the target
src_port=0
dst_port=1337
protocol="TCP"
tool="FUZZ"
desc="FUZZ2"
extension=".pcap"

tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc)
time.sleep(3)
packet = IP(dst=(ip_dst))/fuzz(TCP())
srloop(packet, count=5)
print(packet.summary)
time.sleep(3)
process_kill()













