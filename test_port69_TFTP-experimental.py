#!/usr/bin/env python

'''
### Author: Lenny Miyasato
### 
### The purpose of this script is to help conduct exploratory testing.  It is intended for the operator to set the variables in the variable section and the output will generate files
### in a naming convention.  More info to be added when I finish the concept.
'''

from scapy.all import *
from datetime import datetime
import logging
import random
import string
import os
import time
import signal


'''
# This defines the Source/Target MAC, Target & Port(s)
'''

mac_src="82:ac:18:14:0b:00"			            # Sets your interface to the defined MAC address
ip_src="192.168.0.126"				            # Sets your interface to the defined IP address
ip_dst="127.0.0.1"				                # Sets the IP target that you are testing
interface="eth0"				                # Set this to the interface that you are testing with
pcap_path="/tmp"				                # Sets temporary path to store output files; ensure you have write privs to it
src_host="TEST"                  		        # For filename convention for source hostname
src_target="TFTP-SERVER"                		# For filename convention for target hostname
state="PARKED"                          	    # For filename convention [state|flight]
time_now = datetime.now()                   	# The "time_now" variable sets up the 
time_now = time_now.strftime("%Y-%m-%d-%H-%M")  # time for the filename convention


'''
### These will define various script variables
'''

### This defines the tshark function
def tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc):
    filename = src_host + "_to_" + src_target + "_" + state + "_" + tool + "_" + protocol + "_" + time_now + "_" + desc + extension
    tshark_cmd = "tshark -i" + interface + ' -w ' + pcap_path + "/" + filename + "&"
    os.system(tshark_cmd)
    print("\n \033[92m [*] TSHARK - process successfully started\n \033[97m")

### This defines the nmap function for a UDP scan
def nmap_launch_udp(src_host,src_target,state,tool,protocol,time_now,desc,src_port,dst_port,ip_dst):
    filename = src_host + "_to_" + src_target + "_" + state + "_" + tool + "_" + protocol + "_" + time_now + "_" + desc 
    nmap_cmd_udp = "nmap -sU -Pn -n --max-rate 200 -g " + src_port + " -p " + dst_port + " " + ip_dst + " -oA " + pcap_path + "/" + filename
    os.system(nmap_cmd_udp)
    print("\n \033[92m[*] NMAP - process successfully started\n \033[97m")
    
### This defines the nmap function for a TCP scan
def nmap_launch_tcp(src_host,src_target,state,tool,protocol,time_now,desc,src_port,dst_port,ip_dst):
    filename = src_host + "_to_" + src_target + "_" + state + "_" + tool + "_" + protocol + "_" + time_now + "_" + desc 
    nmap_cmd_tcp = "nmap -sS -Pn -n --max-rate 200 -g " + src_port + " -p " + dst_port + " " + ip_dst + " -oA " + pcap_path + "/" + filename
    print("\n \033[92m[*] NMAP - process successfully started\n\033[97m")
    
### This defines the hping function for a TCP/ICMP ping
def hping_launch(src_host,src_target,state,tool,protocol,time_now,desc,src_port,dst_port,ip_dst):
    filename = src_host + "_to_" + src_target + "_" + state + "_" + tool + "_" + protocol + "_" + time_now + "_" + desc  
    hping_cmd_udp = "hping3 " + ip_dst + " --count 11 --udp --baseport " + src_port + " --destport " + "++" + dst_port + " --keep"                          
    os.system(hping_cmd_udp)


### This section defines the character ramdomization to create payloads of random characters
def randstr(chars = string.ascii_uppercase + string.digits, N=10):
    return ''.join(random.choice(chars) for _ in range(N))
big_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=60000))
med_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=10000))
sml_payload=(randstr(chars='zxcvbnm,./asdfghjkl;qwertyuiop[]1234567890-=ZXCVBNM<>?ASDFGHJKL:QWERTYUIOP{}!@#$%^&*()_+',N=100))

### This dection defines the function to kill a process; used to kill tshark after a command is executed
def process_kill():
        processname = "tshark"
        try:
            # iterating through each instances of the process list
            for line in os.popen("ps ax | grep " + (processname) + " | grep -v grep"):
                fields = line.split()
                pid = fields[0]         # extracting process ID from the output
                os.kill(int(pid), signal.SIGKILL)   # terminating process
            print("\n \033[92m [*] TSHARK - process successfully terminated\n \033[97m ")
        except:
            print("\n \033[91m [X] TSHARK - Uh oh... the process thing didnt work, troubleshoot futher\n \033[97m ")



### This changes the MAC address
def changemac(mac_src,interface):
    mac_down = "ifconfig " + interface + " down" 
    mac_change = "ifconfig " + interface + " hw ether " + mac_src
    mac_up = "ifconfig " + interface + " up " + " & " + "ifconfig " + interface + " |grep ether" 
    os.system(mac_down)
    os.system(mac_change)
    os.system(mac_up)

changemac(mac_src,interface) ### This will execute the changemac function upon start of the script

### This reverts the MAC address back to the original MAC address
def revertmac(interface):
    mac_down = "ifconfig " + interface + " down" 
    mac_revert = "ifconfig " + interface + " hw ether $(ethtool -P " + interface + "|awk '{print $3}')"
    mac_up = "ifconfig " + interface + " up " + " & " + "ifconfig " + interface + " |grep ether" 
    mac_confirm = "ethtool -P " + interface
    os.system(mac_down)
    os.system(mac_revert)
    os.system(mac_up)
    print("\n \033[92m [*] CONFIRM - Ensure your MAC address is reverted \n\033[97m  The " + interface + " address is: ")
    os.system(mac_confirm)

	# ifconfig eth0 hw ether $(ethtool -P eth0 |awk '{print $3}')


def settings_confirmation(ip_src,ip_dst,interface,pcap_path,src_host,src_target,state):
    clear = "clear"
    os.system(clear)
    print("\n \033[94m [*] Please validate your testing script configuration:  \n\033[97m  \n")
    mac_confirm = "ethtool -P " + interface
    print("Your MAC address is:\n\033[92m") 
    os.system(mac_confirm)
    print("\n \033[97m Source IP address is:\033[92m		" + ip_src)
    print("\n \033[97m Target IP address is:\033[92m		" + ip_dst)
    print("\n \033[97m Interface is:\033[92m			" + interface)            
    print("\n \033[97m Output path is:\033[92m		" + pcap_path)
    print("\n \033[97m Source hostname is:\033[92m		" + src_host) 
    print("\n \033[97m Target hostname is:\033[92m		" + src_target)
    print("\n \033[97m State (Parked|Flight) is:\033[92m	" + state)           
    print("\n\n\n")
    input("Press any key to continue")
    
settings_confirmation(ip_src,ip_dst,interface,pcap_path,src_host,src_target,state)


''' #####################################################################################################################
### 		This section is for the test cases
''' #####################################################################################################################


''' #####################################################################################################################
### 		This section is for SCAPY/FUZZ tests
''' #####################################################################################################################


# This sends a big payload of junk to the target on port 69 
src_port=61695
dst_port=69
protocol="UDP"
tool="FUZZ"
desc="TFTP_TEST"
extension=".pcap"


# These sets of variables sets up the protocol level (TFTP) settings

opcode_rrq= '\x00\x01'
opcode_wrq= '\x00\x02'
opcode_data= '\x00\x03'
opcode_ack= '\x00\x04'
opcode_err= '\x00\x05'
tftpcmd_ascii= '\x00\x61\x73\x63\x69\x69\x00'
tftpcmd_binary= '\x00\x62\x69\x6e\x61\x72\x79\x00'
tftpcmd_connect='\x00\x63\x6f\x6e\x6e\x65\x63\x74\x00'
tftpcmd_get='\x00\x90\x00\x67\x65\x74\x00'
tftpcmd_mode='\x00\x6d\x6f\x64\x65\x00'
tftpcmd_netascii='\x00\x6e\x65\x74\x61\x73\x63\x69\x69\x00'
tftpcmd_put='\x00\x70\x75\x74\x00'
tftpcmd_quit='\x71\x75\x69\x74'
tftpcmd_rexmnt='\x00\x70\x75\x74\x00'
tftpcmd_rrq='\x00\x72\x72\x71\x00'
tftpcmd_status='\x00\x73\x74\x61\x74\x75\x73\x00'
tftpcmd_timeout='\x00\x74\x69\x6d\x65\x6f\x75\x74\x00'
tftpcmd_trace='\x00\x74\x72\x61\x63\x65\x00'
tftpcmd_verbose='\x00\x76\x65\x72\x62\x6f\x73\x65\x00'
payload= 'A' * 60
null = '\x00'


print ("\n \033[96m [*] LAUNCHING the" + " " + protocol + " " + tool + " " + desc + " test. \n\033[97m")
tshark_launch(interface,pcap_path,src_host,src_target,state,tool,protocol,time_now,desc)
time.sleep(3)


### This sends malformed TFTP packets to the target.  Not sure what it does to the target.
'''
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/big_payload		# This setups the payload
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_ACK(block=50)
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_DATA(block=50)
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_ERROR(errorcode=2,errormsg="ERROR")
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_OACK()
srloop(packet, count=1)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_Option(oname="AA", value="BB")
srloop(packet, count=10)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_Options(options=["AAAAAAAAAAAA "] )
srloop(packet, count=10)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_RRQ(filename='test.txt',mode='octet')
srloop(packet, count=10)	
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_WRQ(filename="test.txt",mode=b'octet')
srloop(packet, count=10)	
'''
packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/Raw(opcode_rrq)/Raw(big_payload)/Raw(tftpcmd_verbose)
srloop(packet, count=1)

# packet = IP(src=(ip_src), dst=(ip_dst))/UDP(sport=int(src_port), dport=(dst_port))/TFTP()/TFTP_write(filename='A',data='AAAAA',server=(ip_src))
# srloop(packet, count=1)	


print(packet.summary)
time.sleep(10)
process_kill()
print ("\n \033[96m [*] COMPLETED the" + " " + protocol + " " + tool + " " + desc + " test. \n\033[97m")




